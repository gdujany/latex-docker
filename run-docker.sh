#!/usr/bin/env bash

docker run -v $(pwd):/working-dir -w /working-dir -u "$(id -u):$(id -g)"  gitlab-registry.cern.ch/gdujany/latex-docker "$@"
