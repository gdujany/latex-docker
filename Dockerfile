FROM opensuse:latest
MAINTAINER Giulio Dujany <giulio.dujany@cern.ch>

RUN zypper --non-interactive install texlive-scheme-full make
